

describe('Admin control over customer orders', () => { })

describe('Allow Business Rules to grant conditional discounts', () => { })

describe('Allow Ephemeral profiles for anonymous orders', () => { })

describe('Catalog Items search/filter/index frontend', () => { })

describe('Checkout Screen', () => { })

describe('Core website with standard marketing/sales pages', () => {

    const adminuser = {username: 'admin', password: 'admin'}

    describe('Admin Login', ()=>{
        beforeEach(()=>{
            cy.visit('/rwm_admin')
        })

        it('allows authorized administrators login and logout', ()=>{
            cy.get('input[name="login"]').type(adminuser.username)
            cy.get('input[name="password"]').type(`${adminuser.password}{enter}`)
            cy.url().should('be', 'http://localsite.com/rwm_admin/backend')
            cy.visit('/rwm_admin/backend/auth/signout')
            cy.url().should('be', 'http://localsite.com/rwm_admin/backend/auth/signin')
        })

        it('prevents invalid login', ()=>{
            cy.get('input[name="login"]').type('badusername')
            cy.get('input[name="password"]').type(`${adminuser.password}{enter}`)
            cy.get('.flash-message.error')
            cy.url().should('not.be', 'http://localsite.com/rwm_admin/backend')
        })
    })


    describe('Basic Frontend Frame', () => {
        before(() => {
            cy.visit('/')
        })

        describe('Page Meta', () => {
            it('has site title in window title', () => {
                cy.get('head title')
                    .should('contain.text', 'Kowabunga Comics')
            })
        })

        describe('Page Frame', () => {
            it('has nav container with theme name', () => {
                cy.get('*[data-t="nav-outer"]').as('nav-outer')
                cy.get('@nav-outer').get('.navbar-brand')
                    .should('have.text', 'Kowabunga Theme')
            })

            it('has tomato text', () => {
                cy.get('p.lead')
                    .should('have.css', 'color', 'rgb(255, 99, 71)')
            })

            it('has id\'d header and footer', () => {
                cy.get('header')
                    .should('have.id', 'layout-header')
                    .and('have.css', 'background-color', 'rgba(0, 0, 0, 0)')

                cy.get('footer')
                    .should('have.id', 'layout-footer')
            })
        })
    })

})

describe('Customer Order Management', () => { })

describe('Customer Profiles', () => { })

describe('Integrate with Authorize.net', () => { })

describe('Intelligent support for Catalog Items and their availabilities', () => { })

describe('Register Mailgun sender for consistent delivery of critical messaging', () => { })

describe('ShipStation Integration', () => { })

describe('Stock management, back-house', () => { })

describe('Support bulk import', () => { })

describe('Support csv export for publisher orders', () => { })

describe('User Subscriptions, Stored Searches', () => { })

