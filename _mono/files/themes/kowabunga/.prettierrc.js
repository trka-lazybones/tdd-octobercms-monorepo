module.exports = {
  singleQuote: true,
  tabWidth: 2,
  printWidth: 80,
  useTabs: false,
  bracketSpacing: true,

  overrides: [
    {
      files: ['*.{js,ts}'],
      options: {
        semi: false,
        quoteProps: 'consistent',
        arrowParens: 'avoid',
      },
    },
    {
      files: ['*.scss'],
      options: {},
    },
  ],
}
