module.exports = {
  'options': ['setClasses'],
  'feature-detects': [
    'test/css/customproperties',
    'test/css/flexbox',
    'test/css/flexwrap',
    'test/css/cssgrid',
    'test/dom/intersection-observer',
  ],
}
