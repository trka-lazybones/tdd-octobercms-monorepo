const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const BundleAnalyzer = require('webpack-bundle-analyzer').BundleAnalyzerPlugin
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const WebpackMd5Hash = require('webpack-md5-hash')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
var ModernizrWebpackPlugin = require('modernizr-webpack-plugin')

const { prod_path, src_path } = require('./path')
const modernizrConfig = require('../.modernizrrc.js')

module.exports = {
    entry: {
        main: `./${src_path}/scripts/main.ts`, // styles and other global assets
        common: `./${src_path}/scripts/common.ts`, // global ts
        polyfills: `./${src_path}/scripts/polyfills.ts`, // legacy browser support
    },
    resolve: {
        extensions: ['.ts', '.js'],
        alias: {},
    },
    output: {
        path: path.resolve(__dirname, prod_path),
        filename: '[name].js',
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: 'ts-loader',
                exclude: /node_modules/,
            },
            {
                test: /\.(png|svg|jpg|gif)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[name].[ext]',
                        },
                    },
                ],
            },
        ],
    },
    plugins: [
        new ModernizrWebpackPlugin(modernizrConfig),
        new MiniCssExtractPlugin({
            filename: '[name].css',
        }),
        new BundleAnalyzer({
            analyzerMode: 'static',
            openAnalyzer: false,
        }),
        new CleanWebpackPlugin({
            cleanOnceBeforeBuildPatterns: 'resources/**'
        }),
        new WebpackMd5Hash(),
    ],
}
