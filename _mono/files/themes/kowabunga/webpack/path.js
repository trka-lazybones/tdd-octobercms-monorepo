const prod_path = '../resources'
const src_path = 'src'

module.exports = {
  prod_path,
  src_path,
}
