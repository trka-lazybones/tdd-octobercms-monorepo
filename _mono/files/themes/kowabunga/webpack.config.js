const env = process.env.NODE_ENV
const merge = require('webpack-merge')

module.exports = env => {
  return merge(
    require('./webpack/webpack.common.js'),
    require(`./webpack/webpack.${env}.js`)
  )
}
