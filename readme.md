# tdd octobercms monorepo 


This is a pretty opinionated dev environment for OctoberCMS with TDD by way of Cypress.io.  
layout: 

- `.`: Support Files. Make, environments, docker-compose stack, etc. 
- `_mono`: The monorepo root. 
    - `data`: volume mount for db container 
    - `traefik`: config files for traefik proxy container 
    - `files`: webroot volume mount. 

OctoberCMS' physical files are pre-installed and -configured. Bring up the stack with `make start`, and exec the october install scripts from its webroot.  
`config/docker/**` contains cloud-friendly configs with heavy env checks.  
`themes/kowabunga` has boilerplate dependencies, configs, etc for TDD. It's based _entirely_  off of the default demo theme, and adds: 
- Cypress
    - cypress dependencies and starter/example test scripts. 
    - cypress config for mochawesome reporter and writing tests with typescript. 
- Webpack
    - webpack starter config with typescript and scss - minus HMR/Dev Server - with output to `resources` (assets is visible to all backend users, resources is not). 
    - webpack config is lean and mostly language-agnostic (aside from ts/scss) - should be easy to tailor. 
    - modernizr support with conditionally-queued loading.
