## Usage:
## Default env file is make.dev.env
## To override via bash, call a la `envfile=make.prod.env make check_env`
envfile?=make.dev.env
include $(envfile)

# should map to a server in ~/.ssh/config
remote_host = do-galen-allergy
## Important: Always trailing slashes - rsync.
remote_mono = /home/web/stack/public/app-gaa/
local_mono = /home/kevin/workspaces/projects/site_galen-allergy/stack-galen-allergy/_mono/
files = $(local_mono)/files/
data = $(local_mono)/data/

#------------------------------------------ / push, pull /
push:
	rsync -avzr --exclude ".git" --exclude "node_modules" --exclude "docker" \
		$(files) \
		$(remote_host):$(remote_mono)

pull:
	mkdir -p $(files) && \
	rsync -avzr --exclude ".git" --exclude "node_modules" --exclude "docker" \
		$(remote_host):$(remote_mono) \
		$(files)

#------------------------------------------ / db export, import /
# exec mysqldump against the running db container
db_bak:
	mkdir -p data && \
	docker-compose $(dcfile_args) exec db \
		sh -c 'exec mysqldump $$MYSQL_DATABASE -uroot -p"$$MYSQL_ROOT_PASSWORD"' \
		> $(data)/dump.sql

# restore a backup from db_bak.
# alternatively, since this is also the mount for the container's _initdb, same can be done by
# killing the stack, rm-ing the data dir, and re-launching.
# ...but `make db_restore` sounds crazy-easier
db_restore:
	docker-compose $(dcfile_args) exec -i db \
		sh -c 'exec mysql -uroot -p"$$MYSQL_ROOT_PASSWORD"' < $(data)/dump.sql

#------------------------------------------ / Docker Builds /
#build_all: build_app build_csv
#build_app:
#	docker-compose -f docker-compose.build.yml build app
#build_csv:
#	docker-compose -f docker-compose.build.yml build csvimport

#------------------------------------------ / stack
start:
	docker-compose $(dcfile_args) up -d
recreate:
	docker-compose $(dcfile_args) up -d --force-recreate
stop:
	docker-compose $(dcfile_args) down
logs:
	docker-compose $(dcfile_args) logs -f
config:
	docker-compose $(dcfile_args) config

pull-imgs:
	docker-compose $(dcfile_args) pull
